package yuda.ichbal.app06_2e

import android.app.AlertDialog
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.CursorAdapter
import android.widget.ListAdapter
import android.widget.SimpleCursorAdapter
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_data_matkul.view.*
import kotlinx.android.synthetic.main.frag_data_nilai.*
import kotlinx.android.synthetic.main.frag_data_nilai.view.*
import kotlinx.android.synthetic.main.item_data_matkul.view.*


class FragmentNilai : Fragment(){


    lateinit var thisParent : MainActivity
    lateinit var lsAdapter: ListAdapter
    lateinit var spAdapter: SimpleCursorAdapter
    lateinit var dialog: AlertDialog.Builder
    lateinit var v :View
    var kodeNilai : String=""
    var namaMhs : String=""
    var namaMatkul : String=""
    var namaNilai : String=""
    lateinit var db : SQLiteDatabase

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.frag_data_matkul,container, false)
        db = thisParent.getDbObject()

        return v
    }



}